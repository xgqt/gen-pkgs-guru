#!/bin/sh


# This file is part of gen-pkgs-guru.

# gen-pkgs-guru is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# gen-pkgs-guru is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with gen-pkgs-guru.  If not, see <https://www.gnu.org/licenses/>.


trap 'exit 128' INT
export PATH


here_dir="$(pwd)"
temp_dir="$(mktemp -d)"

timeout=""


if [ -n "${1}" ]
then
    timeout="timeout ${1}"
fi

cd "${temp_dir}"  || exit 1
git clone --depth=1 "https://gitlab.com/src_prepare/gen-pkgs"

cd gen-pkgs  || exit 1
eval "${timeout}" sh ./gen-pkgs.sh "https://github.com/gentoo/guru"

cd "${here_dir}"  || exit 1
mv "${temp_dir}/gen-pkgs/public" .  || exit 1

rm -dr "${temp_dir}/gen-pkgs"
